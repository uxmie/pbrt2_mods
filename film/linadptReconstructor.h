#ifndef __PBRT_LINADPT_RECONS_H
#define __PBRT_LINADPT_RECONS_H

/*
 * The Adaptive Reconstruction implementation, from the paper `Adaptive
 * Rendering with Linear Predictions', 2015, Moon et al.
 *
 * Implementation based on LWRR.
 */
#include <vector>
struct pixInfo {
	int xCoor, yCoor; // The x and y coordinates of the pixel.
	float feature[13]; // List of abstract features.
	float color;//[3]; // The colors are 3 dimensional.
};
class linAdptFilterWindow {
public:
	/* Destructor */
	~linAdptFilterWindow();

	/* Basic info of fthe window */
	int width; //Width of filtering window. Default is 19;
	int maxDimensions; //Maximum dimension. Default is 13.
	int dimension; //The dimension of the feature space
	int centerIdx, xCenter, yCenter; //Center pixel, with its x, y coordinates.

	/* Boundary of the window, relative to the center. */
	int boundLeft, boundRight, boundUp, boundDown;
	int nPixels;

	/* The features in compressed feature space.*/
	pixInfo *featureArray;
	float centerVar; // Variance of the color of center pixel.

	/* Buffers for temporary variables. */
	float *Pbuf, *Gbuf, *PbufCorrection;
	float *tempVector, *accArray;
	std::vector<float> estimates;

	/* Final Answers */
	float *estimatedVector; //One for each channel.
	int optimalWindowSize; //Optimal window size for prediction.

	/* Attempt a reconstruction */
	void reconstructWindow();
//protected:
	/* Add a pixel and update the least square and error
	 * The results are stored in Pbuf and tempVector.
	 */
	void addPixelAndUpdate(int pixelIdx, int width);
	/* Reconstruct a channel */
	//void reconstructChannel();
};

class linAdptReconstructor {
public:
	linAdptReconstructor(int n, int w, int h);
	//:nPixels(n), width(w), height(h);
	~linAdptReconstructor();
	void initReconstructor(float *ii, float *ii2, float *in, float *in2,
	                       float *it, float *it2, float *id, float *id2,
						   int *ispp);

	/* Get reconstructed image pointer */
	inline float *getRecImg() {return outputImg;}

	/* MAIN FUNCTION TO BE CALLED FOR RECONSTRUCTION */
	void reconstructImg();

// I'll make these up as they appear.
//protected:
	/* Compute the actual image and variance. */
	void computeSampleMean();
	/* Extract the features and compress with TSVD. */
	void extractFeatures(int centerPixelIdx, int channel);
	int  svd(float *A, float *V, float *S2, int n);
	/* Apply the optimal filter */
	void applyFilter(int cha);
	linAdptFilterWindow window;

//private:
	/* Input image infomation */
	int nPixels, width, height;

	/* Input image data from the film. The arrays represents the sum of
	 * the data of the images and the sum of the squares. This section
	 * is only modified by the film! */
	float *input_img, *input_img2;       // Color
	float *input_normal, *input_normal2; // Normals
	float *input_tex, *input_tex2;       // Textures
	float *input_depth, *input_depth2;   // Depth
	int *input_SPP;                      // Samples per pixel

	/* Here is our workspace, the actual data we will be manipulating
	 * with their respective variance, used in TSVD. */
	float *img, *varImg;         // Color
	float *normal, *varNormal;   // Normals
	float *texture, *varTexture; // Textures
	float *depth, *varDepth;     // Depth
	float *outputImg;            // Output
	int   *timesPredicted;       // Number of times a pixel is predicted.

	/* The information in the filtering window. */
};

#endif /* end of include guard: __PBRT_LINADPT_RECONS_H */
