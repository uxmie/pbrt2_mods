#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <algorithm>
#include <math.h>
#include "linadptReconstructor.h"
linAdptReconstructor::linAdptReconstructor(int n, int w, int h)
		:nPixels(n), width(w), height(h) {
	/* Allocate memory for each input */
	img            = (float *)calloc(3*n, sizeof(float));
	varImg         = (float *)calloc(3*n, sizeof(float));
	normal         = (float *)calloc(3*n, sizeof(float));
	varNormal      = (float *)calloc(3*n, sizeof(float));
	texture        = (float *)calloc(3*n, sizeof(float));
	varTexture     = (float *)calloc(3*n, sizeof(float));
	depth          = (float *)calloc(  n, sizeof(float));
	varDepth       = (float *)calloc(  n, sizeof(float));
	outputImg      = (float *)calloc(3*n, sizeof(float));
	timesPredicted =   (int *)calloc(3*n, sizeof(float));

	/* Set the memory for the window */
	// The window size is a fixed 19.
	window.width = 19;
	// The maximum dimensions is 13, so we set the maximum size as such.
	window.maxDimensions = 13;
	window.featureArray    =(pixInfo *)calloc(window.width*window.width, sizeof(pixInfo));
	window.estimatedVector = (float *)calloc(window.maxDimensions + 1, sizeof(float));
	window.Pbuf            = (float *)calloc((window.maxDimensions+1)*(window.maxDimensions+1), sizeof(float));
	window.Gbuf            = (float *)calloc(window.maxDimensions + 1, sizeof(float));
	window.tempVector      = (float *)calloc(window.maxDimensions + 1, sizeof(float));
	window.PbufCorrection  = (float *)calloc(window.maxDimensions + 1, sizeof(float));
	window.accArray        = (float *)calloc((window.width>>1) + 1, sizeof(float));

	return;
}

linAdptReconstructor::~linAdptReconstructor() {
	free(img);     free(varImg);
	free(normal);  free(varNormal);
	free(texture); free(varTexture);
	free(depth);   free(varDepth);
	free(outputImg);
	free(timesPredicted);
}

void linAdptReconstructor::initReconstructor(
		float *ii, float *ii2, float *in, float *in2,
		float *it, float *it2, float *id, float *id2,
		int *ispp) {
	/* Copy the input data pointers over to the reconstructor. */
	input_img = ii;    input_img2 = ii2;
	input_normal = in; input_normal2 = in2;
	input_tex = it;    input_tex2 = it2;
	input_depth = id;  input_depth2 = id2;
	input_SPP = ispp;
	return;
}

int linAdptReconstructor::svd(float *A, float *V, float *S2, int n) {
	int  i, j, k, EstColRank = n, RotCount = n, SweepCount = 0;
	int slimit = 6;
	float eps = 1e-7;
	float e2 = 10.f * n * eps * eps;
	float tol = 0.1f * eps;
	float vt, p, x0, y0, q, r, c0, s0, d1, d2;

	while (RotCount != 0 && SweepCount++ <= slimit) {
		RotCount = EstColRank * (EstColRank - 1) / 2;

		for (j = 0; j < EstColRank-1; ++j) {
			for (k = j+1; k < EstColRank; ++k) {
				p = q = r = 0.0;

				for (i = 0; i < n; ++i) {
					x0 = A[i * n + j];
					y0 = A[i * n + k];
					p += x0 * y0;
					q += x0 * x0;
					r += y0 * y0;
				}

				S2[j] = q;
				S2[k] = r;

				if (q >= r) {
					if (q <= e2 * S2[0] || fabs(p) <= tol * q)
						RotCount--;
					else {
						p /= q;
						r = 1.f - r/q;
						vt = sqrtf(4.0f * p * p + r * r);
						c0 = sqrtf(0.5f * (1.f + r / vt));
						s0 = p / (vt*c0);

						// Rotation
						for (i = 0; i < n; ++i) {
							d1 = A[i * n + j];
							d2 = A[i * n + k];
							A[i * n + j] = d1*c0+d2*s0;
							A[i * n + k] = -d1*s0+d2*c0;
						}
						for (i = 0; i < n; ++i) {
							d1 = V[i * n + j];
							d2 = V[i * n + k];
							V[i * n + j] = d1 * c0 + d2 * s0;
							V[i * n + k] = -d1 * s0 + d2 * c0;
						}
					}
				} else {
					p /= r;
					q = q / r - 1.f;
					vt = sqrtf(4.f * p * p + q * q);
					s0 = sqrtf(0.5f * (1.f - q / vt));
					if (p < 0.f)
						s0 = -s0;
					c0 = p / (vt * s0);

					// Rotation
					for (i = 0; i < n; ++i) {
						d1 = A[i * n + j];
						d2 = A[i * n + k];
						A[i * n + j] = d1 * c0 + d2 * s0;
						A[i * n + k] = -d1 * s0 + d2 * c0;
					}
					for (i = 0; i < n; ++i) {
						d1 = V[i * n + j];
						d2 = V[i * n + k];
						V[i * n + j] = d1 * c0 + d2 * s0;
						V[i * n + k] = -d1 * s0 + d2 * c0;
					}
				}
			}
			while (EstColRank >= 3 && S2[EstColRank-1] <= S2[0] * tol + tol * tol)
				EstColRank--;
		}
	}
	return EstColRank;
}

/*
 * This method reconstructs the final image and (eventually) generate the
 * sample map. It is the sole function called by an outside object.
 */
void linAdptReconstructor::reconstructImg() {
	printf("Reconstructing image...\n");

	/* Get the actual image */
	computeSampleMean();
	/*memset(outputImg, 0, 3*nPixels*sizeof(outputImg[0]));
	memset(timesPredicted, 0, 3*nPixels*sizeof(timesPredicted[0]));*/

	/* Find new pixels to reconstruct */
	for(int k = 0; k < 3; k++){
		for(int gran = window.width; gran; gran >>= 1) {
			for (int i = gran >> 1; i < height; i+=gran) {
				for (int j = gran >> 1; j < width; j+=gran) {
					int currentPix = i*width + j;
					if(!timesPredicted[3*currentPix + k]) {
						//printf("Window center: %d %d %d\n", i, j, currentPix);
						extractFeatures(currentPix, k);
						window.reconstructWindow();
						applyFilter(k);
						//TODO: Calculate sample map.
					}
				}
			}
		}
		/* Finished reconstruction. Calculate mean for each pixel for output. */
		for(int i = 0; i < nPixels; ++i) {
			outputImg[3*i + k] /= timesPredicted[3*i + k];
		}
	}
	return;
}

/* Compute the actual image and variance. */
void linAdptReconstructor::computeSampleMean() {
	for(int i = 0, tri = 0; i < nPixels; ++i, tri += 3) {
		float invSPP = 1.0 / input_SPP[i];
		float invSPPvar =invSPP;
		if(input_SPP[i] > 2){
			invSPPvar = 1.0 / (input_SPP[i] - 1);
		}
		//invSPPvar *= invSPPvar;

		img[tri] = input_img[tri] * invSPP;
		img[tri+1] = input_img[tri+1] * invSPP;
		img[tri+2] = input_img[tri+2] * invSPP;
		varImg[tri] = std::max(0.0f, invSPPvar*invSPPvar*(input_img2[tri] - img[tri]*img[tri]*input_SPP[i]));
		varImg[tri+1] = std::max(0.0f, invSPPvar*invSPPvar*(input_img2[tri+1] - img[tri+1]*img[tri+1]*input_SPP[i]));
		varImg[tri+2] = std::max(0.0f, invSPPvar*invSPPvar*(input_img2[tri+2] - img[tri+2]*img[tri+2]*input_SPP[i]));

		normal[tri] = input_normal[tri] * invSPP;
		normal[tri+1] = input_normal[tri+1] * invSPP;
		normal[tri+2] = input_normal[tri+2] * invSPP;
		varNormal[tri] = std::max(0.0f, invSPPvar*(input_normal2[tri] - normal[tri]*normal[tri]*input_SPP[i]));
		varNormal[tri+1] = std::max(0.0f, invSPPvar*(input_normal2[tri+1] - normal[tri+1]*normal[tri+1]*input_SPP[i]));
		varNormal[tri+2] = std::max(0.0f, invSPPvar*(input_normal2[tri+2] - normal[tri+2]*normal[tri+2]*input_SPP[i]));

		texture[tri] = input_tex[tri] * invSPP;
		texture[tri+1] = input_tex[tri+1] * invSPP;
		texture[tri+2] = input_tex[tri+2] * invSPP;
		varTexture[tri] = std::max(0.0f, invSPPvar*(input_tex2[tri] - texture[tri]*texture[tri]*input_SPP[i]));
		varTexture[tri+1] = std::max(0.0f, invSPPvar*(input_tex2[tri+1] - texture[tri+1]*texture[tri+1]*input_SPP[i]));
		varTexture[tri+2] = std::max(0.0f, invSPPvar*(input_tex2[tri+2] - texture[tri+2]*texture[tri+2]*input_SPP[i]));

		depth[i] = input_depth[i] * invSPP;
		varDepth[i] = std::max(0.0f, invSPPvar*(input_depth2[i] - depth[i]*depth[i]*input_SPP[i]));
	}
	return;
}

/* Extract the features and compress with TSVD. */
void linAdptReconstructor::extractFeatures(int centerPixelIdx, int channel) {
	//TODO: Implement TSVD. The current method uses all the features for now.
	int halfWindowWidth = window.width >> 1;
	window.centerIdx = centerPixelIdx;
	window.xCenter = centerPixelIdx % width;
	window.yCenter = centerPixelIdx / width;
	int xCoor = window.xCenter;
	int yCoor = window.yCenter;

	window.boundLeft  = -std::min(xCoor, halfWindowWidth);
	window.boundRight =  std::min(this->width - 1 - xCoor, halfWindowWidth);
	window.boundUp    = -std::min(yCoor, halfWindowWidth);
	window.boundDown  =  std::min(this->height - 1 - yCoor, halfWindowWidth);
	//printf("Window bound: %d %d %d %d\n", window.boundLeft, window.boundRight, window.boundUp, window.boundDown);
	window.nPixels    =  (window.boundRight - window.boundLeft + 1)
	                    *(window.boundUp - window.boundDown + 1);
	window.dimension  =  9;
	window.centerVar = varImg[3*centerPixelIdx + channel];

	//TSVD

	float *featureSVD = (float*)calloc(9*9, sizeof(float));
	float *V = (float*)calloc(9*9, sizeof(float));
	float *S = (float*)calloc(9, sizeof(float));
	float featureSum[9] ={0};
	float factor[9] = {0}, delta[9];

	for(int i = window.boundUp; i <= window.boundDown; i++) {
		for(int j = window.boundLeft; j <= window.boundRight; j++) {
			int current = (i + halfWindowWidth)*window.width + (j + halfWindowWidth);
			window.featureArray[current].xCoor = window.xCenter + j;
			window.featureArray[current].yCoor = window.yCenter + i;

			int currInImg = centerPixelIdx + j + i*(this->width);
			window.featureArray[current].feature[0] = (float)i - halfWindowWidth;
			window.featureArray[current].feature[1] = (float)j - halfWindowWidth;
			window.featureArray[current].feature[2] = normal[3*currInImg] - normal[3*centerPixelIdx];
			window.featureArray[current].feature[3] = normal[3*currInImg + 1] - normal[3*centerPixelIdx + 1];
			window.featureArray[current].feature[4] = normal[3*currInImg + 2] - normal[3*centerPixelIdx + 2];
			window.featureArray[current].feature[5] = texture[3*currInImg] - texture[3*centerPixelIdx];
			window.featureArray[current].feature[6] = texture[3*currInImg + 1] - texture[3*centerPixelIdx + 1];
			window.featureArray[current].feature[7] = texture[3*currInImg + 2] - texture[3*centerPixelIdx + 2];
			window.featureArray[current].feature[8] = depth[currInImg] - depth[centerPixelIdx];

			window.featureArray[current].color = img[3*currInImg + channel];

			for(int k = 0; k < 9; k++){
				featureSum[k] += window.featureArray[current].feature[k];
			}
		}
	}
	for(int i = 0; i < 9; i++) {
		featureSum[i] /= (float)window.nPixels;
		V[i*9 + i] = 1.0;
	}
	for(int i = window.boundUp, idx = 0; i <= window.boundDown; i++) {
		for(int j = window.boundLeft; j <= window.boundRight; j++, idx+= window.width*window.width) {
			int current = (i + halfWindowWidth)*window.width + (j + halfWindowWidth);
			for(int k = 0; k < 9; k++){
				factor[k] = std::max(factor[k],
					fabs(window.featureArray[current].feature[k] - featureSum[k]));
			}
		}
	}
	for(int i = 0; i < 9; i++) {
		factor[i] = 1/std::max(factor[i], 0.01f);
	}
	//printf("Hello.\n");

	float errNorm = 0;
	for(int i = window.boundUp; i <= window.boundDown; i++) {
		for(int j = window.boundLeft; j <= window.boundRight; j++) {
			int current = (i + halfWindowWidth)*window.width + (j + halfWindowWidth);
			int currInImg = centerPixelIdx + j + i*(this->width);
			for(int k = 0; k < 9; k++){
				delta[k] = (window.featureArray[current].feature[k] - featureSum[k])*factor[k];
			}
			for(int k = 0; k < 9; k++){
				for(int l = k; l < 9; l++){
					featureSVD[k*9 + l] += delta[k]*delta[l];
				}
			}
			errNorm += factor[2]*factor[2]*varNormal[3*currInImg];
			errNorm += factor[3]*factor[3]*varNormal[3*currInImg + 1];
			errNorm += factor[4]*factor[4]*varNormal[3*currInImg + 2];
			errNorm += factor[5]*factor[5]*varTexture[3*currInImg];
			errNorm += factor[6]*factor[6]*varTexture[3*currInImg + 1];
			errNorm += factor[7]*factor[7]*varTexture[3*currInImg + 2];
			errNorm += factor[8]*factor[8]*varDepth[currInImg];
		}
	}
	for(int i = 1; i < 9; i++){
		for(int j = 0; j < i; j++){
			featureSVD[i*9+j] = featureSVD[j*9+i];
		}
	}
	int rank = svd(featureSVD, V, S, 9);
	errNorm = sqrtf(errNorm)/(sqrtf(rank)*0.5f);
	errNorm = 2.f*errNorm + 0.01;
	int dim = 0;
	for(dim = 0; dim < 9; dim++){
		if(S[dim] < errNorm) break;
	}
	//printf("Compressing to %d dimensions.\n", dim);
	window.dimension = dim;
	for(int i = window.boundUp, idx = 0; i <= window.boundDown; i++) {
		for(int j = window.boundLeft; j <= window.boundRight; j++, idx+= window.width*window.width) {
			int current = (i + halfWindowWidth)*window.width + (j + halfWindowWidth);
			int currInImg = centerPixelIdx + j + i*(this->width);
			for(int k = 0; k < dim; k++){
				delta[k] = 0.f;
				for(int l = 0; l < 9; l++){
					delta[k] += window.featureArray[current].feature[k]*V[l*9 + k];
				}
			}
			for(int k = 0; k < dim; k++){
				window.featureArray[current].feature[k] = delta[k];
			}
		}
	}
	free(featureSVD);
	free(V);
	free(S);
	//TSVD end

	/*for(int i = window.boundUp; i <= window.boundDown; i++) {
		for(int j = window.boundLeft; j <= window.boundRight; j++) {
			int current = (i + halfWindowWidth)*window.width + (j + halfWindowWidth);
			window.featureArray[current].xCoor = window.xCenter + j;
			window.featureArray[current].yCoor = window.yCenter + i;

			int currInImg = centerPixelIdx + j + i*(this->width);
			window.featureArray[current].feature[0] = (float)i - halfWindowWidth;
			window.featureArray[current].feature[1] = (float)j - halfWindowWidth;
			window.featureArray[current].feature[2] = normal[3*currInImg] - normal[3*centerPixelIdx];
			window.featureArray[current].feature[3] = normal[3*currInImg + 1] - normal[3*centerPixelIdx + 1];
			window.featureArray[current].feature[4] = normal[3*currInImg + 2] - normal[3*centerPixelIdx + 2];
			window.featureArray[current].feature[5] = texture[3*currInImg] - texture[3*centerPixelIdx];
			window.featureArray[current].feature[6] = texture[3*currInImg + 1] - texture[3*centerPixelIdx + 1];
			window.featureArray[current].feature[7] = texture[3*currInImg + 2] - texture[3*centerPixelIdx + 2];
			window.featureArray[current].feature[8] = depth[currInImg] - depth[centerPixelIdx];

			window.featureArray[current].color = img[3*currInImg + channel];
		}
	}*/
}

/* Apply the optimal filter */
void linAdptReconstructor::applyFilter(int cha) {
	/* Find the boundaries of affected pixels */
	int leftLim  = std::max(window.boundLeft, -window.optimalWindowSize);
	int rightLim = std::min(window.boundRight, window.optimalWindowSize);
	int upLim    = std::max(window.boundUp, -window.optimalWindowSize);
	int downLim  = std::min(window.boundDown, window.optimalWindowSize);
	int halfWindowWidth = window.width >> 1;
	//printf("%d %d %d %d\n", upLim, downLim, leftLim, rightLim);
	/*printf("%d Used vector: ", window.centerIdx);
	for(int i = 0; i < window.dimension + 1; i++){
		printf("%f ", window.estimatedVector[i]);
	}
	puts("");*/
	for(int i = upLim; i <= downLim; i++) {
		for(int j = leftLim; j <= rightLim; j++) {
			/* Get the current pixel index in the window and the image */
			int currInImg = window.centerIdx + j + i*(this->width);
			int current = (i + halfWindowWidth)*window.width + (j + halfWindowWidth);
			//printf("%d %d %d %d %d\n", i, j, currInImg, current, window.centerIdx);

			/* For each channel, predict the color */
			float predictedValue = window.estimatedVector[0];
			for(int l = 0; l < window.dimension; l++) {
				predictedValue += window.estimatedVector[l+1]
				                 *window.featureArray[current].feature[l];
				//printf("%f ", window.featureArray[current].feature[l]);
				//printf("(%f %f %f) ", window.estimatedVector[l+1], window.estimatedVector[l+1]*window.featureArray[current].feature[l], predictedValue);
			}
			//printf("\n");
			//printf("%f\n", predictedValue);
			outputImg[3*currInImg + cha] += predictedValue;
			timesPredicted[3*currInImg + cha]++;
		}
	}
}

/* Below are the functions for linAdptFilterWindow */
/* Destructor */
linAdptFilterWindow::~linAdptFilterWindow() {
	free(featureArray);
	free(estimatedVector);
	free(Pbuf);
	free(PbufCorrection);
	free(Gbuf);
	free(tempVector);
	free(accArray);
}
/* Attempt a reconstruction. Basically a wrapper for reconstuctChannel */
void linAdptFilterWindow::reconstructWindow() {
	int centerX = width>>1, centerY = width >> 1;
	int centerPixelIdx = centerX*width + centerY;
	estimates.clear();

	//Start with center pixel
	for(int i = 0; i < (dimension + 1)*(dimension + 1); i++){
		Pbuf[i] = 0;
	}
	for(int i = 0; i < dimension + 1; i++){
		accArray[i] = 0;
	}
	tempVector[0] = featureArray[centerPixelIdx].color;
	estimates.push_back(tempVector[0]);
	Pbuf[0] = 1000;
	for(int i = 1; i < dimension + 1; i++){
		tempVector[i] = 0;
		estimates.push_back(0);
		Pbuf[i*(dimension + 1) + i] = 1000;
	}
	accArray[0] = centerVar;
	//printf("accArray[0]: %f, avgError: %f\n", accArray[0], accArray[0]);
	/*printf("Added pixel no: %d(", centerPixelIdx);
	printf("%f ", featureArray[centerPixelIdx].color[cha]);
	printf(")\nFeatures: ");
	for(int i = 0; i < dimension; i++){
		printf("%f ", featureArray[centerPixelIdx].feature[i]);
	}*/

	//Special case for first iteration. TODO
	if(!boundLeft || !boundRight || !boundUp || !boundDown){
		optimalWindowSize = 0;
		for(int i = 0; i < dimension + 1; i++){
			estimatedVector[i] = estimates[i];
		}
		return;
	}
	addPixelAndUpdate(centerPixelIdx - 1 - width, 1);
	addPixelAndUpdate(centerPixelIdx - 0 - width, 1);
	addPixelAndUpdate(centerPixelIdx + 1 - width, 1);
	addPixelAndUpdate(centerPixelIdx - 1, 1);
	addPixelAndUpdate(centerPixelIdx + 1, 1);
	addPixelAndUpdate(centerPixelIdx - 1 + width, 1);
	addPixelAndUpdate(centerPixelIdx - 0 + width, 1);
	addPixelAndUpdate(centerPixelIdx + 1 + width, 1);
	accArray[1] = 0;
	for(int i = -1; i <= 1; i++){
		for(int j = -1; j <= 1; j++){
			int currentIdx = centerPixelIdx + i*width + j;
			float error = tempVector[0];
			for(int j = 1; j < dimension + 1; j++){
				error += tempVector[j]*featureArray[currentIdx].feature[j - 1];
			}
			error -= featureArray[currentIdx].color;

			float correction = 0;
			for(int k = 0, idx = 0; k < dimension + 1; k++) {
				float corrTemp = Pbuf[idx++];
				for(int l = 1; l < dimension + 1; l++) {
					corrTemp += Pbuf[idx++]*featureArray[currentIdx].feature[l-1];
				}
				if(k) {
					correction += corrTemp*featureArray[currentIdx].feature[k-1];
				} else {
					correction += corrTemp;
				}
			}
			correction = 1 - correction;
			error /= correction;
			accArray[1] += error*error;
		}
	}
	//printf("Current Prediction:\n");
	//for(int i = 0; i < dimension + 1; i++) {
	//	printf("%f ", tempVector[i]);
	//}
	//printf("\naccArray[1]: %f, avgError: %f\n", accArray[1], accArray[1]/9);
	for(int i = 0; i < dimension + 1; i++){
		estimates.push_back(tempVector[i]);
	}

	// General case
	int testWidth;
	for(testWidth = 2; testWidth < (width >> 1) + 1; testWidth++) {
		if(-boundLeft < testWidth || boundRight < testWidth
			 || -boundUp < testWidth || boundDown < testWidth)break;
		accArray[testWidth] = 0;
		addPixelAndUpdate(centerPixelIdx - testWidth - testWidth*width, testWidth);
		addPixelAndUpdate(centerPixelIdx + testWidth - testWidth*width, testWidth);
		addPixelAndUpdate(centerPixelIdx - testWidth + testWidth*width, testWidth);
		addPixelAndUpdate(centerPixelIdx + testWidth + testWidth*width, testWidth);
		for(int j = -testWidth + 1; j < testWidth; j++) {
			addPixelAndUpdate(centerPixelIdx - testWidth + j*width, testWidth);
			addPixelAndUpdate(centerPixelIdx + testWidth + j*width, testWidth);
			addPixelAndUpdate(centerPixelIdx + j - testWidth*width, testWidth);
			addPixelAndUpdate(centerPixelIdx + j + testWidth*width, testWidth);
		}
		accArray[testWidth] += accArray[testWidth - 1];
		for(int j = 0; j < dimension + 1; j++){
			estimates.push_back(tempVector[j]);
		}
		//debug
		//printf("Current Prediction: \n");
		//for(int j = 0; j < dimension + 1; j++)
		//	printf("%f ", tempVector[j]);
		//printf("\naccArray[%d]: %f, avgError: %f\n", testWidth, accArray[testWidth], accArray[testWidth] / ((2*testWidth + 1)*(2*testWidth + 1)));
	}
	float minerror = accArray[testWidth - 1];
	optimalWindowSize = testWidth - 1;
	for(int i = testWidth - 1; i >= 0; i--) {
		float currError = accArray[i] / ((2*i + 1)*(2*i + 1));
		if(currError < minerror && minerror - currError >= 1.e-6) {
			minerror = currError;
			optimalWindowSize = i;
		}
	}
	int offset = optimalWindowSize*(dimension + 1);
	for(int i = 0; i < dimension + 1; i++) {
		estimatedVector[i] = estimates[offset + i];
		//printf("%f ", estimatedVector[i]);
	}
	//puts("");
	//printf("optimalWindowSize : %d\n", optimalWindowSize);
	return;
}

void linAdptFilterWindow::addPixelAndUpdate(int pixelIdx, int testWidth){
	/*printf("\tAdded pixel no: %d(", pixelIdx);
	for(int i = 0; i < 3; i++){
		printf("%f ", featureArray[pixelIdx].color[i]);
	}
	printf(")\n\tFeatures: ");
	for(int i = 0; i < dimension; i++){
		printf("%f ", featureArray[pixelIdx].feature[i]);
	}*/
	/* Update Gbuf */
	for(int i = 0; i < dimension + 1; i++){
		int PbufOffset = i*(dimension + 1);
		Gbuf[i] = Pbuf[PbufOffset];
		for(int j = 0; j < dimension; j++){
			Gbuf[i] += Pbuf[PbufOffset + j + 1]*featureArray[pixelIdx].feature[j];
		}
	}
	float GbufCoef = Gbuf[0];
	for(int i = 0; i < dimension; i++){
		GbufCoef += Gbuf[i + 1] * featureArray[pixelIdx].feature[i];
	}
	GbufCoef = 1.f / (GbufCoef + 1);
	for(int i = 0; i < dimension + 1; i++) {
		Gbuf[i] *= GbufCoef;
	}

	/* Update Pbuf */
	for(int i = 0; i < dimension + 1; i++) {
		PbufCorrection[i] = Pbuf[i];
		for(int j = 0; j < dimension; j++) {
			PbufCorrection[i] += Pbuf[i + (j + 1)*(dimension+1)]*featureArray[pixelIdx].feature[j];
		}
	}
	for(int i = 0, PbufOffset = 0; i < dimension + 1; i++) {
		for(int j = 0; j < dimension + 1; j++, PbufOffset++) {
			Pbuf[PbufOffset] -= Gbuf[i]*PbufCorrection[j];
		}
	}

	/* Update tempVector */
	float tempCorrect = tempVector[0];
	for(int i = 0; i < dimension; i++) {
		tempCorrect += tempVector[i + 1]*featureArray[pixelIdx].feature[i];
	}
	tempCorrect = featureArray[pixelIdx].color - tempCorrect;
	for(int i = 0; i < dimension + 1; i++) {
		tempVector[i] += Gbuf[i]*tempCorrect;
	}

	/* Update accArray */
	int estimatesOffset = (testWidth - 1)*(dimension + 1);
	float error = estimates[estimatesOffset];
	for(int i = 0; i < dimension; i++){
		error += estimates[estimatesOffset + i + 1]*featureArray[pixelIdx].feature[i];
	}
	error -= featureArray[pixelIdx].color;
	accArray[testWidth] += error*error;

	//debug
	/*printf("\n\tGBuf: ");
	for(int i = 0; i < dimension + 1; i++){
		printf("%f ", Gbuf[i]);
	}
	printf("\n\tPBuf: ");
	for(int i = 0; i < (dimension + 1)*(dimension + 1); i++){
		printf("%s%f", i%(dimension + 1)?" ":"\n\t\t", Pbuf[i]);
	}
	printf("\n\ttempVector: ");
	for(int i = 0; i < dimension + 1; i++) {
		printf("%f ", tempVector[i]);
	}
	printf("\n\taccArray[%d]: %f\n", testWidth, accArray[testWidth]);*/

	return;
}

/*int main() {
	int width, height;
	float input[3][10000*3];
	float input2[3][10000*3];
	float depth[10000], depth2[10000];
	int sample[10000];
	scanf("%d%d", &width, &height);
	linAdptReconstructor rec(width*height, width, height);

	for(int i = 0, tri = 0; i < width*height; i++, tri += 3){
		scanf("%f%f%f%f%f%f", &input[0][tri], &input[0][tri+1], &input[0][tri+2],
		                      &input2[0][tri], &input2[0][tri+1], &input2[0][tri+2]);
		scanf("%f%f%f%f%f%f", &input[1][tri], &input[1][tri+1], &input[1][tri+2],
		                      &input2[1][tri], &input2[1][tri+1], &input2[1][tri+2]);
		scanf("%f%f%f%f%f%f", &input[2][tri], &input[2][tri+1], &input[2][tri+2],
		                      &input2[2][tri], &input2[2][tri+1], &input2[2][tri+2]);
		scanf("%f%f%d", &depth[i], &depth2[i], &sample[i]);
	}
	rec.initReconstructor(input[0], input2[0], input[1], input2[1],
	                      input[2], input2[2], depth, depth2, sample);
	rec.reconstructImg();
	float *out = rec.getRecImg();
	for(int i = 0, idx = 0; i < height; i++) {
		for(int j = 0; j < width; j++, idx += 3) {
			printf("(%f %f %f)", out[idx], out[idx + 1], out[idx + 2]);
		}
		puts("");
	}
}*/
