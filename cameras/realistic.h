
#if defined(_MSC_VER)
#pragma once
#endif

#ifndef PBRT_CAMERAS_REALISTIC_H
#define PBRT_CAMERAS_REALISTIC_H

#include "camera.h"
#include "paramset.h"
#include "film.h"
#include <vector>

struct LensSphere{
	bool isApt;
	float zPos;
	float radius;
	float cosPhi;
	float IOR;
	float relIOR;
	LensSphere(float rad, float pos, float ior, float apt);
};

// RealisticCamera Declarations
class RealisticCamera : public Camera {
public:
	// RealisticCamera Public Methods
	RealisticCamera(const AnimatedTransform &cam2world,
						float hither, float yon, float sopen,
						float sclose, float filmdistance, float aperture_diameter, string specfile,
						float filmdiag, Film *film);
	float GenerateRay(const CameraSample &sample, Ray *) const;
  
private:
	// RealisticCamera Private Data
	std::vector<LensSphere> Lens;
	float aperture;
	float filmdist, rfilmdist2;
	float filmZ;
	float filmSizeX, filmSizeY;
	float pupilRadius;
};


RealisticCamera *CreateRealisticCamera(const ParamSet &params,
        const AnimatedTransform &cam2world, Film *film);


#endif	// PBRT_CAMERAS_REALISTIC_H
