#include "cameras/realistic.h"
#include "sampler.h"
#include "montecarlo.h"
#include "stdafx.h"

LensSphere::LensSphere(float rad, float pos, float ior, float apt)
{
	if(rad == 0.){
		isApt = true;
		radius = 0;
		zPos = pos;
		return;
	}

	isApt = false;

	apt = apt/2;
	radius = (rad > 0.)? rad: -rad;
	IOR = ior;
	zPos = pos - rad;
	float adj = sqrt(rad*rad - apt*apt);
	cosPhi = (rad > 0)? adj: -adj; //negative if rad < 0;
}

RealisticCamera::RealisticCamera(const AnimatedTransform &cam2world,
				 float hither, float yon, 
				 float sopen, float sclose, 
				 float filmdistance, float aperture_diameter, string specfile, 
				 float filmdiag, Film *f)
	: Camera(cam2world, sopen, sclose, f) // pbrt-v2 doesnot specify hither and yon
{
   // YOUR CODE HERE -- build and store datastructures representing the given lens
   // and film placement.
	FILE *lensFile;
	char fileBuf[200];
	lensFile = fopen(specfile.c_str(), "r");
	if(lensFile == NULL){
		Severe("Severe error: The lens file was not found.\n");
	}

	float totalLensLength = 0.;
	float rad, nextpos, ior, apt;
	while(fgets(fileBuf, 190, lensFile) != NULL){
		char *dataHead;
		for(dataHead = fileBuf; isblank(*dataHead); dataHead++);
		if(*dataHead == '#') continue; //Line is a comment. Skip to next;
		
		sscanf(dataHead, "%f%f%f%f", &rad, &nextpos, &ior, &apt);
		Lens.push_back(LensSphere(rad, totalLensLength, ior, apt));
		totalLensLength -= nextpos;
	}
	for(int i = (int)Lens.size() - 1; i > 0; i--){
			Lens[i].IOR = Lens[i - 1].IOR;
	}
	Lens[0].IOR = 1.;

	for(unsigned i = 0; i < Lens.size() - 1; i++){
		Lens[i].relIOR = Lens[i + 1].IOR / Lens[i].IOR;
	}
	Lens[Lens.size() - 1].relIOR = 1./Lens[Lens.size() - 1].IOR;

	pupilRadius = apt / 2;
	aperture = aperture_diameter / 2;

	float asp = (float)(film -> yResolution)/(float)(film -> xResolution);
	filmZ = totalLensLength - filmdistance;
	filmSizeX = filmdiag / sqrt(asp*asp + 1);
	filmSizeY = filmSizeX * asp;
	filmdist = filmdistance;

	fclose(lensFile);
	
	//Calculate rfilmdist2
	rfilmdist2 = (pupilRadius*pupilRadius*3.14159)/(filmdist*filmdist);//(filmVec.x*filmVec.x + filmVec.y*filmVec.y + filmVec.z*filmVec.z);

	//Dump camera info (debug)
	//printf("Camera Info: apt:%f; pupil: %f, filmSizeX: %f, filmSizeY: %f",
	//		aperture, pupilRadius, filmSizeX, filmSizeY);
	return;
}

float RealisticCamera::GenerateRay(const CameraSample &sample, Ray *ray) const {
  // YOUR CODE HERE -- make that ray!
  
  // use sample->imageX and sample->imageY to get raster-space coordinates
  // of the sample point on the film.
  // use sample->lensU and sample->lensV to get a sample position on the lens
  //
	//Find point on the film.
	float xRes = (float)(film -> xResolution);
	float yRes = (float)(film -> yResolution);

	Point filmPoint(-filmSizeX*(sample.imageX / xRes - 0.5),
			filmSizeY*(sample.imageY / yRes - 0.5), filmZ);
	
	//Find point on the lens and generate ray.
	float lensU, lensV, cosTheta;
	Ray trav;
	ConcentricSampleDisk(sample.lensU, sample.lensV, &lensU, &lensV);
	lensU *= pupilRadius;
	lensV *= pupilRadius;
	trav.o = filmPoint;
	trav.d = Normalize(Point(lensU, lensV, filmZ + filmdist) - filmPoint);
	cosTheta = trav.d.z;
	
	//Traverse the lenses. Return 0 if blocked.
	float prevIOR = 1.;
	for(int i = (int)Lens.size() - 1; i >= 0; i--){
		//If the lens is the aperture, just find out if it is blocked.
		if(Lens[i].isApt){
			float tHit = (Lens[i].zPos - trav.o.z)/trav.d.z;
			Point pHit = trav(tHit);
			if(pHit.x*pHit.x + pHit.y*pHit.y > aperture*aperture){
				return 0.;
			}
			continue;
		}

		// Solve Quadratic equation
		float qA, qB, qC;
		float cMinz = trav.o.z - Lens[i].zPos;
		qA = 1.;
		qB = 2*(trav.o.x*trav.d.x + trav.o.y*trav.d.y + cMinz*trav.d.z);
		qC = trav.o.x*trav.o.x + trav.o.y*trav.o.y + cMinz*cMinz
			- Lens[i].radius*Lens[i].radius;
		
		float t1, t2;
		if(!Quadratic(qA, qB, qC, &t1, &t2)){
			return 0.;
		}

		// Find if point lies in the surface
		// and get the normal if so.
		float tHit = (Lens[i].cosPhi > 0)? t2: t1;
		Point pHit = trav(tHit);
		Vector hitNor;
		if(Lens[i].cosPhi > 0){
			if((pHit.z - Lens[i].zPos) < Lens[i].cosPhi)
				return 0.;
			hitNor = Normalize(pHit - Point(0, 0, Lens[i].zPos));
		} else {
			if((pHit.z - Lens[i].zPos) > Lens[i].cosPhi)
				return 0.;
			hitNor = Normalize(Point(0, 0, Lens[i].zPos) - pHit);
		}

		// Find and update ray using Snell's Law.

		/*float rRef = Lens[i].IOR/prevIOR; //Other method
		float cosIn = Dot(trav.d, hitNor);
		float Ncoeff = cosIn - sqrt(rRef*rRef + cosIn*cosIn - 1);
		trav.d = (trav.d - Ncoeff*hitNor)/rRef;*/

		//Heckbert's method
		float ref = Lens[i].relIOR;
		float cosIn = Dot(trav.d, hitNor);
		float cosOut = sqrt(1 - ref*ref*(1 - cosIn*cosIn));
		trav.d = ref*trav.d + (cosOut - ref*cosIn)*hitNor;
		trav.o = pHit;
		prevIOR = Lens[i].IOR;
	}

	*ray = CameraToWorld(Ray(trav.o, trav.d, 0.));
	return cosTheta*cosTheta*cosTheta*cosTheta*rfilmdist2;
}


RealisticCamera *CreateRealisticCamera(const ParamSet &params,
        const AnimatedTransform &cam2world, Film *film) {
	// Extract common camera parameters from \use{ParamSet}
	float hither = params.FindOneFloat("hither", -1);
	float yon = params.FindOneFloat("yon", -1);
	float shutteropen = params.FindOneFloat("shutteropen", -1);
	float shutterclose = params.FindOneFloat("shutterclose", -1);

	// Realistic camera-specific parameters
	string specfile = params.FindOneString("specfile", "");
	float filmdistance = params.FindOneFloat("filmdistance", 70.0); // about 70 mm default to film
 	float fstop = params.FindOneFloat("aperture_diameter", 1.0);	
	float filmdiag = params.FindOneFloat("filmdiag", 35.0);

	Assert(hither != -1 && yon != -1 && shutteropen != -1 &&
		shutterclose != -1 && filmdistance!= -1);
	if (specfile == "") {
	    Severe( "No lens spec file supplied!\n" );
	}
	return new RealisticCamera(cam2world, hither, yon,
				   shutteropen, shutterclose, filmdistance, fstop, 
				   specfile, filmdiag, film);
}
