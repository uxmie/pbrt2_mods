
/*
    pbrt source code Copyright(c) 1998-2012 Matt Pharr and Greg Humphreys.

    This file is part of pbrt.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:

    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */


// lights/medianCutEnvironmentLight.cpp*
#include "lights/medianCutEnvironmentLight.h"
#include "stdafx.h"
#include "sh.h"
#include "montecarlo.h"
#include "paramset.h"
#include "imageio.h"
#include <queue>

// MedianCutEnvironmentLight Utility Classes
float MedianCutEnvironmentLight::pixalEnergyEval(const RGBSpectrum &s) const {
	float eval[3];
	s.ToRGB(eval);
	return (0.2125*eval[0] + 0.7154*eval[1] + 0.0721*eval[2]);
}

float MedianCutEnvironmentLight::sumAreaEval(float *sumAreaTable,
		int startX, int startY, int endX, int endY) const {
	float ret = sumAreaTable[endY*width + endX];
	if(startX) ret -= sumAreaTable[endY*width + startX - 1];
	if(startY) ret -= sumAreaTable[(startY - 1)*width + endX];
	if(startY && startX) ret += sumAreaTable[(startY - 1)*width + startX - 1];
	return ret;
}

int MedianCutEnvironmentLight::getCut(float *sumAreaTable,
		cutArea &area, bool *dimension) const {	
	int start, end;
	float desiredIntensity = sumAreaEval(sumAreaTable,
			area.startX, area.startY, area.endX, area.endY)/2;
	float areaHeight = (float)(area.endY - area.startY);
	float areaWidth = (float)(area.endX - area.startX)
		*sinf((float)(area.startY + area.endY)/(2.*height)*M_PI);
	//printf("Evaluating (%d %d) %d %d %f\n", //debug
	//		area.startX, area.startY, area.endX, area.endY, desiredIntensity*2);
	if(areaWidth > areaHeight){
		*dimension = 0;
		start = area.startX;
		end = area.endX;
		if(start == end){ //Cut area is only one pixel wide and cannot be cut.
			return -1;
		}
		while(start < end){
			int mid = (start + end)>>1;
			float eval = sumAreaEval(sumAreaTable,
					area.startX, area.startY, mid, area.endY);
			if(eval >= desiredIntensity){
				end = mid;
			} else {
				start = mid + 1;
			}
		}
		if(start == area.endX) start--;
	} else {
		*dimension = 1;
		start = area.startY;
		end = area.endY;
		if(start == end){
			return -1;
		}
		while(start < end){
			int mid = (start + end)>>1;
			float eval = sumAreaEval(sumAreaTable,
					area.startX, area.startY, area.endX, mid);
			if(eval >= desiredIntensity){
				end = mid;
			} else {
				start = mid + 1;
			}
		}
		if(start == area.endY) start--; //We want that both sides of the cut have some area.
	}
	return start;
}

void MedianCutEnvironmentLight::SphericalToDir(const float theta, const float phi, Vector *dir) const {
	float r = sinf(theta);
	float x = r*cosf(phi), y = r*sinf(phi), z = cosf(theta);
	dir -> x = x; dir -> y = y; dir -> z = z;
	return;
}

// MedianCutEnvironmentLight Method Definitions
MedianCutEnvironmentLight::~MedianCutEnvironmentLight() {
    //delete distribution;
    //delete radianceMap;
	delete[] texels;
}


MedianCutEnvironmentLight::MedianCutEnvironmentLight(const Transform &light2world,
        const Spectrum &L, int ns, const string &texmap)
    : Light(light2world, ns) {
    //int width = 0, height = 0;
    //RGBSpectrum *texels = NULL;
	texels = NULL;
    // Read texel data from _texmap_ into _texels_
    if (texmap != "") {
        texels = ReadImage(texmap, &width, &height);
        if (texels)
            for (int i = 0; i < width * height; ++i)
                texels[i] *= L.ToRGBSpectrum();
    }
    if (!texels) {
        width = height = 1;
        texels = new RGBSpectrum[1];
        texels[0] = L.ToRGBSpectrum();
    }
	//radianceMap = new MIPMap<RGBSpectrum>(width, height, texels);

	//printf("Image size: %d %d\n", width, height);
	//Create sum-area map
	float *radSumArea = new float[width*height];
	for(int i = 0; i < height; i++){
		float rad = 0;
		for(int j = 0; j < width; j++){
			rad += sinf((float)i/(float)height*M_PI)
				*pixalEnergyEval(texels[i*width + j]);
			if(i){
				radSumArea[i*width + j]
					= radSumArea[(i - 1)*width + j] + rad;
			} else {
				radSumArea[i*width + j] = rad;
			}
		}
	}


	//Find splits using binary search
	std::queue<cutArea> bfsFindCut;
	bfsFindCut.push(cutArea(0, 0, width - 1, height - 1, 1));
	while(bfsFindCut.front().level < nSamples){
		bool dim;
		cutArea curr = bfsFindCut.front();
		int cutPlace = getCut(radSumArea, curr, &dim);
		if(cutPlace >= 0){
			if(dim == 0){ //vertical
				cutArea split1(curr.startX, curr.startY,
						cutPlace, curr.endY, curr.level<<1); 
				cutArea split2(cutPlace + 1, curr.startY,
						curr.endX, curr.endY, curr.level<<1);
				bfsFindCut.push(split1);
				bfsFindCut.push(split2);
				//printf("Split along x = %d\n", cutPlace);
			} else {
				cutArea split1(curr.startX, curr.startY,
						curr.endX, cutPlace, curr.level<<1); 
				cutArea split2(curr.startX, cutPlace + 1,
						curr.endX, curr.endY, curr.level<<1);
				bfsFindCut.push(split1);
				bfsFindCut.push(split2);
				//printf("Split along y = %d\n", cutPlace);
			}
		} else {
			curr.level <<= 1;
			curr.lights <<= 1;
			bfsFindCut.push(curr);
		}
		bfsFindCut.pop();
	}
	//Fill in the lights
	while(!bfsFindCut.empty()){
		cutArea curr = bfsFindCut.front();
		Spectrum currL;
		float torqueX = 0., torqueY = 0.;
		//printf("%d %d %d %d\n", curr.startX, curr.startY, curr.endX, curr.endY);
		for(int i = curr.startY; i <= curr.endY; i++){
			for(int j = curr.startX; j <= curr.endX; j++){
				RGBSpectrum pixWeight = texels[i*width + j]
					*sinf((float)i / (float)height * M_PI);
				currL += pixWeight;
				torqueX += pixalEnergyEval(pixWeight)*j;
				torqueY += pixalEnergyEval(pixWeight)*i;
			}
		}
		//float centerX = (float)(curr.startX + curr.endX)/2.;
		//float centerY = (float)(curr.startY + curr.endY)/2.;
		float centerX = torqueX / pixalEnergyEval(currL);
		float centerY = torqueY / pixalEnergyEval(currL);
		currL *= 2*M_PI*M_PI/(float)(width*height);
		currL *= 1./(float)curr.lights;
		//printf("%d %d %d %d\n", curr.startX, curr.startY, curr.endX, curr.endY);
		float debug[3];
		currL.ToRGB(debug);
		Vector dir;
		SphericalToDir(centerY / (float)height * M_PI,
				centerX / (float)width * 2 * M_PI, &dir);
		dir = LightToWorld(dir);
		for(int i = 0; i < curr.lights; i++){
			//printf("Creating Light with intensity:%f %f %f %f %f\nand direction %f %f %f\n"
			//		, debug[0], debug[1], debug[2]
			//		, pixalEnergyEval(currL)
			//		, sumAreaEval(radSumArea, curr.startX, curr.startY, curr.endX, curr.endY, height, width)
			//		, dir[0], dir[1], dir[2]);
			printf("Creating Light with intensity:%f\n"
					, pixalEnergyEval(currL)
					);
			cutLights.push_back(dirLight(dir, currL));
		}
		bfsFindCut.pop();
	}
	
    delete[] radSumArea;
	//delete[] texels;
}


Spectrum MedianCutEnvironmentLight::Power(const Scene *scene) const {
    Point worldCenter;
    float worldRadius;
    scene->WorldBound().BoundingSphere(&worldCenter, &worldRadius);
	Spectrum ret;
	for(int i = 0; i < nSamples; i++){
		ret += cutLights[i].L;
	}
	ret *= M_PI * worldRadius * worldRadius;
	return ret;
}


MedianCutEnvironmentLight *CreateMedianCutEnvironmentLight(const Transform &light2world,
        const ParamSet &paramSet) {
    Spectrum L = paramSet.FindOneSpectrum("L", Spectrum(1.0));
    Spectrum sc = paramSet.FindOneSpectrum("scale", Spectrum(1.0));
    string texmap = paramSet.FindOneFilename("mapname", "");
    int nSamples = paramSet.FindOneInt("nsamples", 1);
    if (PbrtOptions.quickRender) nSamples = max(1, nSamples / 4);
    return new MedianCutEnvironmentLight(light2world, L * sc, nSamples, texmap);
}


Spectrum MedianCutEnvironmentLight::Sample_L(const Point &p, float pEpsilon,
        const LightSample &ls, float time, Vector *wi, float *pdf,
        VisibilityTester *visibility) const {
	int pick = rand() % nSamples;
	*wi = cutLights[pick].dir;
	*pdf = 1./(float)nSamples;
    visibility->SetRay(p, pEpsilon, *wi, time);
	//printf("%d, %f %f %f %f\n", pick, wi->x, wi->y, wi->z, pixalEnergyEval(cutLights[pick].L));
	return cutLights[pick].L;

}


float MedianCutEnvironmentLight::Pdf(const Point &, const Vector &w) const {
	return 0.;
}

Spectrum MedianCutEnvironmentLight::Le(const RayDifferential &r) const {
	Vector wh = Normalize(WorldToLight(r.d));
	float s = SphericalPhi(wh) * INV_TWOPI * width;
	float t = SphericalTheta(wh) * INV_PI * height;
	int pickTexel = (int)(s) + (int)(t)*width;

	return Spectrum(texels[pickTexel], SPECTRUM_ILLUMINANT);
	//return Spectrum(radianceMap->Lookup(s, t), SPECTRUM_ILLUMINANT);
}

Spectrum MedianCutEnvironmentLight::Sample_L(const Scene *scene,
        const LightSample &ls, float u1, float u2, float time,
        Ray *ray, Normal *Ns, float *pdf) const {

    // Choose point on disk oriented toward infinite light direction
    Point worldCenter;
    float worldRadius;
    scene->WorldBound().BoundingSphere(&worldCenter, &worldRadius);
    Vector v1, v2;
	int pick = rand()%nSamples;
	
    CoordinateSystem(cutLights[pick].dir, &v1, &v2);
    float d1, d2;
    ConcentricSampleDisk(ls.uPos[0], ls.uPos[1], &d1, &d2);
    Point Pdisk = worldCenter + worldRadius * (d1 * v1 + d2 * v2);

    // Set ray origin and direction for infinite light ray
    *ray = Ray(Pdisk + worldRadius * cutLights[pick].dir,
			-cutLights[pick].dir, 0.f, INFINITY, time);
    *Ns = (Normal)ray->d;

    *pdf = 1.f / (M_PI * worldRadius * worldRadius * nSamples);
    return cutLights[pick].L;
}

