
/*
    pbrt source code Copyright(c) 1998-2012 Matt Pharr and Greg Humphreys.

    This file is part of pbrt.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:

    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */


// shapes/Heightfield2.cpp*

#include "stdafx.h"
#include "shapes/heightfield2.h"
#include "shapes/trianglemesh.h"
#include "paramset.h"

// Heightfield2 Method Definitions
Heightfield2::Heightfield2(const Transform *o2w, const Transform *w2o,
        bool ro, int x, int y, const float *zs)
    : Shape(o2w, w2o, ro) {
    nx = x;
    ny = y;
	rnx = 1./(float)(nx - 1);
	rny = 1./(float)(ny - 1);
    z = new float[nx*ny];
    memcpy(z, zs, nx*ny*sizeof(float));

	//Find vector normals and range of z
	int vNumber = 0;
	zMin = z[0];
	zMax = z[0];
	
	zNormal = NULL; //FOR DEBUGGING AND BENCHMARKING ONLY!
	zNormal = new Normal[nx*ny];
	for(int i = 0; i < ny; i++) {
		for(int j = 0; j < nx; j++, vNumber++) {
			Normal temp(0., 0., 0.);
			if(i != ny - 1 && j != nx - 1){
				temp += Normalize(Normal(
						(z[vNumber] - z[vNumber + 1])*rny,
						(z[vNumber + 1] - z[vNumber + nx + 1])*rnx,
						rnx*rny));
				temp += Normalize(Normal(
						(z[vNumber + nx] - z[vNumber + nx + 1])*rny,
						(z[vNumber] - z[vNumber + nx])*rnx,
						rnx*rny));
			}
			if(i != ny - 1 && j){
				temp += Normalize(Normal(
						(z[vNumber - 1] - z[vNumber])*rny,
						(z[vNumber] - z[vNumber + nx])*rnx,
						rnx*rny));
			}
			if(i && j){
				temp += Normalize(Normal(
						(z[vNumber - 1] - z[vNumber])*rny,
						(z[vNumber - nx - 1] - z[vNumber - 1])*rnx,
						rnx*rny));
				temp += Normalize(Normal(
						(z[vNumber - nx - 1] - z[vNumber - nx])*rny,
						(z[vNumber - nx] - z[vNumber])*rnx,
						rnx*rny));
			}
			if(i && j != nx - 1){
				temp += Normalize(Normal(
						(z[vNumber] - z[vNumber + 1])*rny,
						(z[vNumber - nx] - z[vNumber])*rnx,
						rnx*rny));
			}
			zNormal[vNumber] = Normalize(temp);
		}
	}

	for(int i = 0; i < ny; i++) {
		for(int j = 0; j < nx; j++, vNumber++) {
			if(z[vNumber] > zMax) zMax = z[vNumber];
			else if(z[vNumber] < zMin) zMin = z[vNumber];
		}
	}
}


Heightfield2::~Heightfield2() {
	delete[] z;
	if(zNormal != NULL)delete[] zNormal;
}


BBox Heightfield2::ObjectBound() const {
    float minz = z[0], maxz = z[0];
    for (int i = 1; i < nx*ny; ++i) {
        if (z[i] < minz) minz = z[i];
        if (z[i] > maxz) maxz = z[i];
    }
    return BBox(Point(0,0,minz), Point(1,1,maxz));
}


bool Heightfield2::CanIntersect() const {
    return true;
}

/* This function finds if a Triangle defined by e1, e2 and s
 * intersects a ray r in barycentric coordinates. 
 * Used in Heightfield2::Intersect()
 */
bool Heightfield2::TrisIsect(const Ray &r, float *tHit,  float *bari1, float *bari2,
		const Vector &e1, const Vector &e2, const Vector &d) const {
	Vector s1 = Cross(r.d, e2);

	float divisor = Dot(s1, e1);
	if(divisor == 0.)
		return false;
	float invdivisor = 1.f/divisor;
	
	float b1 = Dot(d, s1) * invdivisor;
	if(b1 < 0. || b1 > 1.)
		return false;

	Vector s2 = Cross(d, e1);
	float b2 = Dot(r.d, s2) * invdivisor;
	if(b2 < 0. || b1 + b2 > 1.)
		return false;
	
	float t = Dot(e2, s2) * invdivisor;
	if(t < r.mint || t > r.maxt)
		return false;
	
	*tHit = t;
	*bari1 = b1;
	*bari2 = b2;
	return true;
}

bool Heightfield2::Intersect(const Ray &r, float *tHit,
		float *rayEpsilon, DifferentialGeometry *dg) const {
	//Tranform Ray to object space
	Ray inRay;
	(*WorldToObject)(r, &inRay);
	
	//Find inRay.mint and inRay.maxt
	float minTX, minTY, minTZ, maxTX, maxTY, maxTZ, minVoxelT, maxVoxelT;
	float rdx = 1.f/inRay.d.x, rdy = 1.f/inRay.d.y, rdz = 1.f/inRay.d.z;
	int boxX = nx - 1, boxY = ny - 1, gridX, gridY;
	Point start = inRay(inRay.mint);
	
	minTX = (-inRay.o.x)*rdx;
	maxTX = (1.f - inRay.o.x)*rdx;
	if(minTX > maxTX) swap(minTX, maxTX);
	minTY = (-inRay.o.y)*rdy;
	maxTY = (1.f - inRay.o.y)*rdy;
	if(minTY > maxTY) swap(minTY, maxTY);
	minTZ = (zMin - inRay.o.z)*rdz;
	maxTZ = (zMax - inRay.o.z)*rdz;
	if(minTZ > maxTZ) swap(minTZ, maxTZ);
	
	minVoxelT = max(max(max(minTX, minTY), 0.8f*minTZ), inRay.mint);
	maxVoxelT = min(min(min(maxTX, maxTY), 1.2f*maxTZ), inRay.maxt);
	//minVoxelT = max(max(minTX, minTY), inRay.mint);
	//maxVoxelT = min(min(maxTX, maxTY), inRay.maxt);
	//if(inRay.mint > inRay.maxt) return false;
	//fprintf(stderr, "%f %f %f \n\t%f %f %f\n\t%f %f %f\n\t%f %f %f %f\n",
	//		minTX, minTY, minTZ,
	//		maxTX, maxTY, maxTZ,
	//		inRay.d.x, inRay.d.y, inRay.d.z,
	//		inRay.mint, inRay.maxt, minVoxelT, maxVoxelT);
	if(minVoxelT > maxVoxelT) return false;

	//Convert starting position to grid coordinates
	start = inRay(minVoxelT);
	gridX = floor(start.x*boxX);
	if(gridX < 0) gridX = 0;
	else if (gridX >= boxX) gridX = boxX - 1;
	gridY = (int)(start.y*boxY);
	if(gridY < 0) gridY = 0;
	else if (gridY >= boxY) gridY = boxY - 1;
	
	//Traverse height field
	float currentT = minVoxelT, nextT;
	int incX, incY;
	bool dirX = (inRay.d.x > 0.)? 1: 0;
	bool dirY = (inRay.d.y > 0.)? 1: 0;

	while(gridX >=0 && gridY >= 0 && gridX < boxX && gridY < boxY) {
		int vNumber = gridY * nx + gridX;
		float t1, t2, b1, b2;
		bool isect1, isect2;
		
		float nextTX = ((float)(gridX + dirX)*rnx - inRay.o.x)*rdx;
		float nextTY = ((float)(gridY + dirY)*rny - inRay.o.y)*rdy;
		if(nextTX < nextTY){
			nextT = nextTX;
			incX = dirX? 1: -1;
			incY = 0;
		} else {
			nextT = nextTY;
			incY = dirY? 1: -1;
			incX = 0;
		}

		//If the ray doesn't pass through the current voxel at all,
		//Skip it.
		float gridMinZ = z[vNumber], gridMaxZ = z[vNumber], rayMinZ, rayMaxZ;
		if(gridMinZ > z[vNumber + 1]) gridMinZ = z[vNumber + 1];
		if(gridMinZ > z[vNumber + nx]) gridMinZ = z[vNumber + nx];
		if(gridMinZ > z[vNumber + nx + 1]) gridMinZ = z[vNumber + nx + 1];
		if(gridMaxZ < z[vNumber + 1]) gridMaxZ = z[vNumber + 1];
		if(gridMaxZ < z[vNumber + nx]) gridMaxZ = z[vNumber + nx];
		if(gridMaxZ < z[vNumber + nx + 1]) gridMaxZ = z[vNumber + nx + 1];
		rayMinZ = inRay(currentT).z;
		rayMaxZ = inRay(nextT).z;
		if(rayMinZ > gridMaxZ + 0.01 || rayMaxZ < gridMinZ - 0.01){
			gridX += incX; gridY += incY; currentT = nextT;
			continue;
		}
		
		Vector lowerRighte1(-rnx, 0.f, z[vNumber] - z[vNumber + 1]),
			   lowerRighte2(0.f, rny, z[vNumber + nx + 1] - z[vNumber + 1]),
			   lowerRightes(inRay.o - Point((float)(gridX + 1)*rnx, (float)(gridY)*rny, z[vNumber + 1]));
		isect1 = TrisIsect(inRay, &t1, &b1, &b2,
				lowerRighte1, lowerRighte2, lowerRightes);
		
		Vector upperLefte1(rnx, 0., z[vNumber + nx + 1] - z[vNumber + nx]),
			   upperLefte2(0., -rny, z[vNumber] - z[vNumber + nx]),
			   upperLeftes(inRay.o - Point((float)gridX*rnx, (float)(gridY + 1)*rny, z[vNumber + nx]));
		isect2 = TrisIsect(inRay, &t2, &b1, &b2,
				upperLefte1, upperLefte2, upperLeftes);

		if((isect1 && !isect2) || (isect1 && isect2 && t1 <= t2)){
			//Return differential geometry.
			*tHit = t1;
			const Transform &o2w = *ObjectToWorld;
			Vector dpdu = o2w(Vector(1., 0., (-lowerRighte1.z*boxX)));
			Vector dpdv = o2w(Vector(0., 1., lowerRighte2.z*boxY));
			float tu = ((float)gridX + 1. - b1)*rnx;
			float tv = ((float)gridY + b2)*rny;
			*dg = DifferentialGeometry(o2w(inRay(t1)), dpdu, dpdv,
					Normal(0,0,0), Normal(0,0,0), tu, tv, this);
			*rayEpsilon = 1e-3f * t1;
			return true;
		} else if((!isect1 && isect2) || (isect1 && isect2)) {
			//Return differential geometry.
			*tHit = t2;
			const Transform &o2w = *ObjectToWorld;
			Vector dpdu = o2w(Vector(1., 0., upperLefte1.z*boxX));
			Vector dpdv = o2w(Vector(0., 1., (-upperLefte2.z*boxY)));
			float tu = ((float)gridX + b1)*rnx;
			float tv = ((float)gridY + 1 - b2)*rny;
			*dg = DifferentialGeometry(o2w(inRay(t2)), dpdu, dpdv,
					Normal(0,0,0), Normal(0,0,0), tu, tv, this);
			*rayEpsilon = 1e-3f * t2;
			return true;
		}

		//Move to next block.
		gridX += incX; gridY += incY; currentT = nextT;
		if(currentT > maxVoxelT) break;
	}
	return false;
}

bool Heightfield2::IntersectP(const Ray &r) const {
	//Tranform Ray to object space
	Ray inRay;
	(*WorldToObject)(r, &inRay);
	
	//Find inRay.mint and inRay.maxt
	float minTX, minTY, minTZ, maxTX, maxTY, maxTZ, minVoxelT;
	float rdx = 1.f/inRay.d.x, rdy = 1.f/inRay.d.y, rdz = 1.f/inRay.d.z;
	int boxX = nx - 1, boxY = ny - 1, gridX, gridY;
	Point start = inRay(inRay.mint);
	
	minTX = (-inRay.o.x)*rdx;
	maxTX = (1.f - inRay.o.x)*rdx;
	if(minTX > maxTX) swap(minTX, maxTX);
	minTY = (-inRay.o.y)*rdy;
	maxTY = (1.f - inRay.o.y)*rdy;
	if(minTY > maxTY) swap(minTY, maxTY);
	minTZ = (zMin - inRay.o.z)*rdz;
	maxTZ = (zMax - inRay.o.z)*rdz;
	if(minTZ > maxTZ) swap(minTZ, maxTZ);
	
	minVoxelT = max(max(minTX, minTY), inRay.mint);
	inRay.mint = max(inRay.mint, (float)0.8*minTZ);
	inRay.maxt = min((float)1.2*maxTZ, inRay.maxt);
	if(inRay.mint > inRay.maxt) return false;
	
	//Quick hack using the intermediate value theorem.
	if(inRay.mint == minTZ && inRay.maxt == maxTZ)
		return true;

	//Convert starting position to grid coordinates
	start = inRay(minVoxelT);
	gridX = floor(start.x*boxX);
	if(gridX < 0) gridX = 0;
	else if (gridX >= boxX) gridX = boxX;
	gridY = (int)(start.y*boxY);
	if(gridY < 0) gridY = 0;
	else if (gridY >= boxY) gridY = boxY;
	
	//Traverse height field
	float currentT = minVoxelT, nextT;
	int incX, incY;
	bool dirX = (inRay.d.x > 0.)? 1: 0;
	bool dirY = (inRay.d.y > 0.)? 1: 0;
	
	while(gridX >=0 && gridY >= 0 && gridX < boxX && gridY < boxY) {
		int vNumber = gridY * nx + gridX;
		float t1, t2, b1, b2;
		
		float nextTX = ((float)(gridX + dirX)*rnx - inRay.o.x)*rdx;
		float nextTY = ((float)(gridY + dirY)*rny - inRay.o.y)*rdy;
		if(nextTX < nextTY){
			nextT = nextTX;
			incX = dirX? 1: -1;
			incY = 0;
		} else {
			nextT = nextTY;
			incY = dirY? 1: -1;
			incX = 0;
		}
		float gridMinZ = z[vNumber], gridMaxZ = z[vNumber], rayMinZ, rayMaxZ;
		if(gridMinZ > z[vNumber + 1]) gridMinZ = z[vNumber + 1];
		if(gridMinZ > z[vNumber + nx]) gridMinZ = z[vNumber + nx];
		if(gridMinZ > z[vNumber + nx + 1]) gridMinZ = z[vNumber + nx + 1];
		if(gridMaxZ < z[vNumber + 1]) gridMaxZ = z[vNumber + 1];
		if(gridMaxZ < z[vNumber + nx]) gridMaxZ = z[vNumber + nx];
		if(gridMaxZ < z[vNumber + nx + 1]) gridMaxZ = z[vNumber + nx + 1];
		rayMinZ = inRay(currentT).z;
		rayMaxZ = inRay(nextT).z;
		if(rayMinZ > gridMaxZ + 0.01 || rayMaxZ < gridMinZ - 0.01){
			gridX += incX; gridY += incY; currentT = nextT;
			continue;
		}
		
		Vector lowerRighte1(-rnx, 0.f, z[vNumber] - z[vNumber + 1]),
			   lowerRighte2(0.f, rny, z[vNumber + nx + 1] - z[vNumber + 1]),
			   lowerRightes(inRay.o - Point((float)(gridX + 1)*rnx, (float)(gridY)*rny, z[vNumber + 1]));
		if(TrisIsect(inRay, &t1, &b1, &b2,
				lowerRighte1, lowerRighte2, lowerRightes)){
			return true;
		}
		
		Vector upperLefte1(rnx, 0., z[vNumber + nx + 1] - z[vNumber + nx]),
			   upperLefte2(0., -rny, z[vNumber] - z[vNumber + nx]),
			   upperLeftes(inRay.o - Point((float)gridX*rnx, (float)(gridY + 1)*rny, z[vNumber + nx]));
		if(TrisIsect(inRay, &t2, &b1, &b2,
				upperLefte1, upperLefte2, upperLeftes)){
			return true;
		}
		//Move to next block.
		gridX += incX; gridY += incY; currentT = nextT;
		if(currentT > inRay.maxt) break;
	}
	return false;
}


void Heightfield2::GetShadingGeometry(const Transform &obj2world,
		const DifferentialGeometry &dg, DifferentialGeometry *dgShading) const {
	if(zNormal == NULL){
		*dgShading = dg;
		return;
	}
	
	// Determine grid number and triangle from uv.
	int gridX = floor(dg.u*(nx - 1));
	int gridY = floor(dg.v*(ny - 1));
	if(gridX == nx - 1)gridX--;
	if(gridY == ny - 1)gridY--;
	bool triType = ((dg.u - gridX*rnx)*rny - (dg.v - gridY*rny)*rnx < 0);

	//Get barycentric coordinates.
	float b0, b1, b2;
	Normal zN[3];
	if(triType){
		b1 = dg.u*(nx - 1) - (float)gridX;
		b2 = (float)(gridY + 1) - dg.v*(ny - 1);
		zN[0] = zNormal[(gridY + 1) * nx + gridX];
		zN[1] = zNormal[(gridY + 1) * nx + gridX + 1];
		zN[2] = zNormal[gridY * nx + gridX];
	} else {
		b1 = (float)(gridX + 1) - dg.u*(nx - 1);
		b2 = dg.v*(ny - 1) - (float)gridY;
		zN[0] = zNormal[gridY * nx + gridX + 1];
		zN[1] = zNormal[gridY * nx + gridX];
		zN[2] = zNormal[(gridY + 1) * nx + gridX + 1];
	}
	b0 = 1. - b1 - b2;
	
	//Get normal and normals and vectors
	Normal ns = Normalize(obj2world(b0*zN[0] + b1*zN[1] + b2*zN[2]));
	Vector ss = Normalize(dg.dpdu),
		   ts = Normalize(Cross(ss, ns));
	ss = Cross(ts, ns);

	//Get dndu and dndv
	Normal dndu, dndv;
	if(triType){
		dndu = (nx - 1)*(zN[1] - zN[0]);
		dndv = (1 - ny)*(zN[2] - zN[0]);
	} else {
		dndu = (1 - nx)*(zN[1] - zN[0]);
		dndv = (ny - 1)*(zN[2] - zN[0]);
	}

	//Return shading geometry
	*dgShading = DifferentialGeometry(dg.p, ss, ts,
			(obj2world)(dndu), (obj2world)(dndv),
			dg.u, dg.v, dg.shape);
}


Heightfield2 *CreateHeightfield2Shape(const Transform *o2w, const Transform *w2o,
        bool reverseOrientation, const ParamSet &params) {
    int nu = params.FindOneInt("nu", -1);
    int nv = params.FindOneInt("nv", -1);
    int nitems;
    const float *Pz = params.FindFloat("Pz", &nitems);
    Assert(nitems == nu*nv);
    Assert(nu != -1 && nv != -1 && Pz != NULL);
    return new Heightfield2(o2w, w2o, reverseOrientation, nu, nv, Pz);
}


